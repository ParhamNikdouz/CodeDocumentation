const Author = require('../models/Author');
const upash = require('upash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

module.exports = function (app) {

    app.get('/login', function (req, res, next) {
        res.render('login', {success: req.session.success, errors: req.session.errors, title: "ورود", layout: false});
        req.session.errors = null;
    });


	passport.use(new LocalStrategy(
        function (username, password, done) {
            Author.getAuthorByAuthorname(username, function(err, author) {
                    if(err) throw err;
                    if(!author) {
                        return done(null, false, { message: 'نام کاربری یا رمز عبور اشتباه است.' });
                    }
                    
                    Author.comparePassword(password, author.password, function(err, isMatch) {
                            if(err) throw err;
                            if (isMatch) {
                                return done(null, author);
                            } else {
                                return done(null, false, { message: 'نام کاربری یا رمز عبور اشتباه است.' });
                            }
                            });

                    });
         }));

	passport.serializeUser(function(author, done) {
	  done(null, author.id);
	});

	passport.deserializeUser(function(id, done) {
	  Author.getAuthorById(id, function(err, author) {
		done(err, author);
	  });
	});

    app.post('/login', passport.authenticate('local', { successRedirect: 'dashboard', failureRedirect:'/login', failureFlash: true }),
            function(req, res) {
                res.redirect('dashboard');
    });

    app.get('/logout', function(req, res) {
            req.logout();

            req.flash('success_msg', 'با موفقیت خارج شدید.');

            res.redirect('/login');
    });

    app.get("/dashboard", ensureAuthenticated, function (req, res) {
        res.render("dashboard" , {title : "داشبورد"});
    });
    function ensureAuthenticated (req, res, next) {
        if(req.isAuthenticated()) {
            return next();
        } else {
            req.flash('error_msg', 'باید وارد سایت شوید.');
            res.redirect('/login');
        }
    }
};

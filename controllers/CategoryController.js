const Category = require("../models/Category");

module.exports = (app) => {

    // Add GET
    app.get('/category/add', ensureAuthenticated, (req, res) => {
        res.render('category/add', {title: "دسته بندی | افزودن"})
    });

    // Add POST
    app.post('/category/add', (req, res) => {
        let newCategory = new Category(req.body);

        newCategory.save((err) => {
            if (!err) {
                res.render('category/add', {
                    message: {
                        success: true,
                        text: "با موفقیت ثبت شد"
                    },
                    title: "دسته بندی ها | افزودن"
                });
            }
            else {
                console.log(`Error During Save Category :
                            ${err.message}`);
                res.render('category/add', {
                    message: {
                        success: false,
                        text: "خطا در هنگام ثبت",
                    },
                    title: "دسته بندی ها | افزودن"
                });
            }
        });
    });

    // Edit GET
    app.get('/category/edit/:id', ensureAuthenticated, (req, res) => {

        Category.findById(req.params.id, function (err, category) {
            if (err) {
                console.log(`En Error Occurred :
                            ${err.message}`);
            } else {
                res.render('category/edit', { category: category, title: "دسته بندی ها | ویرایش"});
            }
        })

    });
    //
    // Edit POST
    app.post('/category/edit', (req, res) => {
        let newCategory = {title : req.body.title};

        Category.updateOne({ _id: req.body.id}, newCategory, (err) => {
            if (err) {
                // TODO: Fix this part
                console.log(`Error During Updating Category :
                            ${err.message}`);
                newCategory.id = id;
                res.render('category/edit', {
                    message: {
                        success: false,
                        text: "خطا در هنگام ویرایش",
                    },
                    title: "دسته بندی ها | افزودن",
                    category: newCategory
                });

            } else {
                res.redirect("list");
            }
        });
    });

    // List GET
    app.get('/category/list', ensureAuthenticated, (req, res) => {
        Category.find({}, [],
            {
                sort: {
                    order: 1 //Sort by Date Added DESC
                }
            }, function (err, categories) {
                if (err) {
                    console.log("An Error Occurred : ", err.message);
                    res.render("category/list", {
                        categories: {}, message: {
                            success: false,
                            text: "خطا در ارتباط با سرور"
                        }, title: "دسته بندی ها | لیست"
                    });
                } else {
                    res.render("category/list", {categories: categories, title: "دسته بندی ها | لیست"});
                }
            })
    });

    // Delete GET
    app.get('/category/delete/:id', ensureAuthenticated, (req, res) => {
        let categoryId = req.params.id;
        if (categoryId) {
            Category.findById(categoryId).remove(function (err) {
                if (err) {
                    console.log(`An Error Occurred :
                                 ${err.message}`);

                    res.json({
                        success: false,
                        text: "خطا در حین حذف کردن"
                    });
                } else {
                    res.json({
                        success: true,
                        text: "با موفقیت حذف شد"
                    });
                }

            })
        }
    });
    function ensureAuthenticated (req, res, next) {
        if(req.isAuthenticated()) {
            return next();
        } else {
            req.flash('error_msg', 'باید وارد سایت شوید.');
            res.redirect('/login');
        }
    }
};

const mongoose = require('mongoose');

let CategorySchema = mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    order:{
        type: Number
    }
});

module.exports = mongoose.model('Category', CategorySchema);
